Manager bibliografii integrat cu EasyBib - Tema 23 

Membri: Marius-Octavian Predusca

Specificatii:
    - aplicatie web formata din parte de frontend si backend;
    - utilizare HTML, CSS si Javascript si API-ul de pe site-ul easyBib;
    - aplicatia va comunica cu site-ul easyBib prin fisiere JSON.
    - proiectul va reusi sa stocheze diferite formate de bibliografii, organizandu-le pe mai multe categorii in functie de preferintele utilizatorului, in baza de date;
    - in aplicatie va exista si o sectiune unde se va putea scrie text care va folosi citatele stocate in baza de date sau cu ajutorul site-ului;
    - aplicatia va avea un formular prin care se vor forma bibliografii noi (acest formular va trimite raw data catre easyBib si i se va returna in schimb bibliografia prelucrata);
    - aplicatia va reusi sa diferentieze dintre un citat dintr-un site web, carte, jurnal etc;
    - TODO
    

Descriere:

    Aplicatiile de management de bibliografii au ca scop principal stocarea si organizarea diferitelor referinte ce se pun la finalul citatelor.
    O alta functionalitate este utilizarea mult mai usoara a citatelor in timpul scrierii unui articol, noi trebuind doar sa accesam numele autorului citatului.
(ex. Scriem un citat al lui Mihai Eminescu, noi prin aplicatie doar cautam numele lui in baza de date si ne alegem  datele concrete despre respectivul citat 
intr-un format predefinit de noi).